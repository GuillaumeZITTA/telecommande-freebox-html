# Telecommande HTML PC freebox

Ceci est une télécommande freebox implémentée dans une page web statique qui peut êêtre utile si vous ne voulez/pouvez pas utiliser une application mobile comme [zapette](https://play.google.com/store/apps/details?id=com.leonid.zapette&hl=fr).

## Usage

* Trouvez votre code de télécommande dans la freebox dans le menu : "Réglages -> Système -> Informations Freebox Player et Server"
* Téléchargez le fichier HTML sur votre PC (ou clonez ce dépot)
* Ouvrir le fichier dans un navigateur récent/décent (pas IE)
* Mettre le code dans l'url, exemple : ```file:///home/user/Desktop/telecommande-freebox-html/FreeboxRemote.html?code=90159019&box=hd1```
* Essayer
* Enregistrer la page dans vos favoris / signets etc...

## Problemes / pas implementé

* Les appuis "long" ne sont pas gérés
* On arrive pas à allumer la box : Il faut aller dans la gestion d'alimentation et mettre "mise en veille" pour l'action du bouton eteindre
  * quand la box est vraiment éteinte, seule une vrai télécommande peut l'allumer
